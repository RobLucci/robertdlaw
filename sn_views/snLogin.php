<?php
$sn = socialNetwork::getInstance();

?>

<div id="container">       
    <div class="login">
        <?php echo $sn->error; ?>
        <form id="log_in" method="POST" action="<?php filterSERVER('PHP_SLEF'); ?>">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Login</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            <label for="username">User name:</label>
                        </th>
                        <td>
                            <input type="text" value="<?php escape(isset($_POST['username'])); ?>" name="username" id="username" class="login" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="password">Password:</label>
                        </th>
                        <td>
                            <input type="password" value="<?php escape(isset($_POST['password'])); ?>" name="password" id="password" class="login" />
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <input type="submit" name="login" value="Log in" />
                        </td>
                        <td>
                        </td>
                    </tr>              
                </tfoot>
            </table>
        </form>
    </div>               
<script type="text/javascript" src="js/loginFocus.js"></script>
</div><!-- end of container div -->