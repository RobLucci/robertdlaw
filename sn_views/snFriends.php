<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();
?>

<div class="main">

<?php
$sn->showProfile($sn->view);

$sn->mutual();
$sn->followers();
$sn->following();

if(!$sn->friendChecker)
{
    echo "You don't have any friends yet.";
}
?>

<a class="button" href="messages?view=<?php echo $sn->view; ?>"> View <?php echo $sn->user2; ?> message</a>

</div>