<div class="main"><h3>Please enter your details to sign up</h3></div>

<?php 

$sn = socialNetwork::getInstance();

if(isset($_SESSION['user']))
{
    destroySession();
}
?>   
    <div class="register">    
        <form id="register" method="POST" action="<?php filterSERVER('PHP_SELF'); ?>" >
            <table>    
                <tr>
                    <th>
                        <label for="username">User name:</label>
                    </th>
                    <td>
                        <input type="text" value="<?php if(isset($_POST['username'])) echo escape($_POST['username']); ?>" name="username" id="username" class="signup" maxlength="32" onblur="checkUser(this);" />
                    </td>
                    <td>
                        <span id="info"></span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="firstname">First name:</label>
                    </th>
                    <td>
                        <input type="text" value="<?php if(isset($_POST['firstname'])) echo escape($_POST['firstname']); ?>" name="firstname" id="firstname" class="signup" maxlength="32" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="lastname">Last name:</label>
                    </th>
                    <td>
                        <input type="text" value="<?php if(isset($_POST['lastname'])) echo escape($_POST['lastname']); ?>" name="lastname" id="lastname" class="signup" maxlength="32" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="password">Password:</label>
                    </th>
                    <td>
                        <input type="password" name="password" id="password" class="signup" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="repassword">Repeat Password:</label>
                    </th>
                    <td>
                        <input type="password" name="repassword" id="repassword" class="signup" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="email">Email:</label>
                    </th>
                    <td>
                        <input type="email" value="<?php if(isset($_POST['email'])) echo escape($_POST['email']); ?>" name="email" id="email" class="signup" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan = '3'>
                    <input type="submit" name="signup" value="Sign up" class="signup" />
                    </td>
                </tr>
            </table>
        </form>
    </div> 

<script>
    function checkUser(username)
    {
        if(username.value == '')
        {
            O('info').innerHTML = '';
            return;
        }
        
        params = "username="+username.value;
        request = new ajaxRequest();
        request.open("POST", "snCheckUser.php", true);
        request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        request.send(params);
        
        request.onreadystatechange = function()
        {
            if(this.readyState == 4)
            {
                if(this.status == 200)
                {
                    if(this.responseText != null)
                    {
                        O('info').innerHTML = this.responseText;
                    }
                }
            }
        }
    }
    
    function ajaxRequest()
    {
        try
        {
            var request = new XMLHttpRequest();
        }
        catch(e1)
        {
            try
            {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch(e2)
            {
                try
                {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch(e3)
                {
                    request = false;
                }
            }
        }
        return request;
    }
</script>
