<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();

if(isset($_GET['view']))
{
?>
<div id='footer'>
    <p class='footerpara'>
        Copyright &#169; 2014 all rights reserved.
        No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
        By accessing any information beyond this page, you agree to abide by this Privacy Policy.
    </p>
</div>
</body>
</html>

<?php
die();
}
?>

<h3 class="members">Other Members</h3>

<?php

$sn->members();

?>