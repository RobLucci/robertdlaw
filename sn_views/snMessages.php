<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();

if($sn->view != "")
{
    $sn->messages2();
?>
<div class="main">

    <h3><?php echo $sn->user1; ?> Messages</h3>
   <?php $sn->showProfile($sn->view);?>
    
    <form id="messages" method="POST" action="messages?view=<?php echo $sn->view; ?>">
        <p>Type here to leave a message:</p>
        <textarea name="text" class="textarea"></textarea>
        <label>Public
        <input type="radio" name="pm" value="0" checked="checked" />
        </label>
        <label>Private
        <input type="radio" name="pm" value="1" />
        </label>
        <div>
        <input type="submit" value="Post Message"> 
        </div>
    </form>
    <div class='board'>
<?php 
$sn->messages3();
}

if(!$sn->num)
{
?>
    
    <span class="info">No messages yet.</span>
    
<?php 

}
?>
    <a class="info" href="messages?view=<?php echo $sn->view; ?>">Refresh messages</a>
    <a class="button" href="friends?view=<?php echo $sn->view; ?>">View <?php echo $sn->user2; ?> friends </a>
    </div>
</div>