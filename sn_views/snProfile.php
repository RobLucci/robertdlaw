<?php
$sn = socialNetwork::getInstance();
?>

<div class="main">
    <h3>Your Profile</h3>
    <?php $sn->showProfile($sn->username); ?>
    
    <form id="profile" method="POST" action="<?php filterSERVER('PHP_SELF'); ?>" enctype="multipart/form-data" >
        <h3>Enter or edit your details and/or upload an image</h3>
        <textarea name="text" cols="50" rows="3"><?php $sn->text; ?></textarea><br />
        <p>Image:</p>
        <input type="file" name="image" size="14" maxlength="32"/>
        <input type="submit" value="Save Profile" />
    </form>
    
</div>