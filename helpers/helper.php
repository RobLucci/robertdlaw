<?php
/*
 * by Robert
 *
*********/

// render template

function renderTemplate($template, $data = array()) {

    //sanitize argument
    $sTemplate = htmlspecialchars($template);

    $directory = "../templates/$sTemplate.php";

    if(file_exists($directory))
    {
        extract($data);
        require($directory);
    }
    else
    {
        echo 'The template:'.' '.$sTemplate.' '.'does not exist.<br />';
    }
}

function renderView($view, $data = array()) {

    //sanitize argument
    $sView = htmlspecialchars($view);

    $directory = "../views/$sView.php";

    if(file_exists($directory))
    {
        extract($data);
        require($directory);
    }
    else
    {
        echo 'The template:'.' '.$sView.' '.'does not exist.<br />';
    }
}

// render models

function renderModel($model) {

    //sanitize argument
    $sModel = htmlspecialchars($model);

    $directory = "../models/$sModel.php";

    if(file_exists($directory))
    {
        require($directory);
    }
    else
    {
        echo 'The model:'.' '.$sModel.' '.'does not exist.<br />';
    }
}

// prevent SQL injection [Not working]
function mysqli_fix_string($string) 
{
    if(get_magic_quotes_gpc()) 
    {
        $string = stripslashes($string);
    }    
    return mysqli_real_escape_string( /*need connection as first parameter*/ $string);
}


//Prevent HTML injection
function mysqli_entities_fix_string()
{
    return htmlspecialchars(mysqli_fix_string($string));
}

//Sanitized input from users
function escape($string)
{
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

//log out script
if (isset($_GET["logout"]))
{
    escape($_GET['logout']);
    session_start();
    session_destroy();
}

//reroute function
function reroute($page)
{
    $host = escape($_SERVER['HTTP_HOST']);
    $path = rtrim(dirname(escape($_SERVER['PHP_SELF'])), " /\\ ");
    header("Location: http://$host$path/$page");
}

// filtering the superglobal POST
function filterPOST($varibleName)
{
    return filter_input(INPUT_POST,$varibleName,FILTER_SANITIZE_STRING );
}

// filtering the superglobal SERVER
function filterSERVER($varibleName)
{
    return filter_input(INPUT_SERVER,$varibleName,FILTER_SANITIZE_STRING );
}

// filtering the superglobal GET
function filterGET($varibleName)
{
    return filter_input(INPUT_GET,$varibleName,FILTER_SANITIZE_STRING );
}

// outputs messages in a div 
function output($string) 
{
    echo "<div id=\"output\">" . $string . "</div>";
}

