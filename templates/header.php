<!DOCTYPE html>
<html lang="en">
<head>
<?php
    require_once ('../helpers/helper.php');
    require_once ('../models/classes/DB.class.php');
    require_once ('../models/classes/admin.class.php');
    require_once ('../models/classes/rssFeed.class.php');
?>
    <title>
        <?= $title; ?>
    </title>

    <meta name="<?=$name;?>" content="<?=$content;?>" />
    <meta name="<?=$name2;?>" content="<?=$content2;?>" />
    <meta name="<?=$name3;?>" content="<?=$content3;?>" />
    <meta http-equiv="<?=$httpequiv;?>" content="<?=$content4;?>" />
    <meta charset="UTF-8" />

    <link href="../css/stylecss.css" style="text/css" rel="stylesheet"/>

    <!-- Google analytic script code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-48755887-1', 'robertdlaw.tk');
        ga('send', 'pageview');

    </script>

</head>
<body>

<div id="page">

    <header>

        <h1 class="title"> <?= $title; ?> </h1>

        <a href="index.php"><img src="../images/logo.png" alt="Logo of the site" class="head_img"/></a>

        <nav>
            <ul>
                <li><a href="index"> Home </a></li>
                <li><a href="robert"> Robert </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="http://nti11407245.hosting.warkscol.ac.uk/" target="_blank"> My work </a></li>
                    </ul>
                </li>
                <li><a href="nestim"> NesTim </a></li>
                <li><a href="mail_form"> Contact me </a></li>
                <li><a href="extra"> Extra </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="cv"> Curriculum </a></li>
                        <li><a href="cleveland"> Fort William </a></li>
                    </ul><!-- end of subnav unordered list -->
                </li>
                <li><a href="admin"> Admin area </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="#"> Mini-apps </a><span class="rarrow">&#9654;</span>
                            <ul class="subnav2">
                                <li><a href="customer_form"> Customer Form </a></li>
                                <li><a href="equipment_form"> Equip. Form </a></li>
                            </ul><!-- end of subnav2 unordered list -->
                        </li>
                    </ul><!-- end of subnav unordered list -->
                </li>
            </ul><!-- end of unordered list -->
        </nav> <!-- end of nav -->
    </header> <!-- end of header -->

