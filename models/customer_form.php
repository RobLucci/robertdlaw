<?php

if (isset($_POST["submit"]))

    if (empty($_POST["customer_id"]) ||
        empty($_POST["first_name"]) ||
        empty($_POST["last_name"]) ||
        empty($_POST["address"]) ||
        empty($_POST["email"]) ||
        empty($_POST["city"]) ||
        empty($_POST["phone_number"] ))
    {
        echo "You must fill in all the fields!!!";
    }
    else
    {
        $customer_id = $_POST["customer_id"];
        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $address = $_POST["address"];
        $email = $_POST["email"];
        $city = $_POST["city"];
        $phone_number = $_POST["phone_number"];


        $file = 'customer_form.csv';
        $data = $customer_id. ',' .$first_name. ',' .$last_name. ',' .$address. ',' .$email. ',' .$city. ',' .$phone_number;

        $fp = fopen($file, 'a+');
        $fw = fwrite($file,$data.PHP_EOL);
        fclose($fp);

        echo 'Customer form has been stored successfully !!!!';
    }
