<?php

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['email']) || isset($_POST['fname']) || isset($_POST['lname']) || isset($_POST['subject']) || isset($_POST['textarea']))
    {
        $email = htmlspecialchars(trim($_POST['email']));
        $fname = htmlspecialchars(trim($_POST['fname']));
        $lname = htmlspecialchars(trim($_POST['lname']));
        $subject = htmlspecialchars(trim($_POST['subject']));
        $textarea = htmlspecialchars(trim($_POST['textarea']));

        if (empty($email) || empty($fname) || empty($lname) || empty($subject) || empty($textarea))
        {
            echo '<p style="color: red; text-align: center;"> Please fill all the fields prior sending </p>';
        }
        else
        {
            $my_email = 'robyntimjnr@yahoo.it';

            mail($my_email, $subject, $textarea, 'From:' .$email);             

            echo "<p style='text-align: center; color: green'> Thanks Mr./Mrs./Mss. $lname $fname for your E-mail, I will be in touch with you shortly.</p>";
        }
    }
}

