<?php
if (isset($_POST["submit"]))
{
    if( empty($_POST["equipment_id"]) ||
        empty($_POST["customer_id"]) ||
        empty($_POST["technician"]) ||
        empty($_POST["device"]) ||
        empty($_POST["operation_system"]) ||
        empty($_POST["fault"]) ||
        empty($_POST["system_check"]) ||
        empty($_POST["bios"]) ||
        empty ($_POST["hardware_test"]) ||
        empty ($_POST["repaired"]) ||
        empty($_POST["part_replaced"]) ||
        empty ($_POST["paid"]) ||
        empty($_POST["date_hand_in"]) ||
        empty($_POST["date_hand_out"]) ||
        empty($_POST["receipt"]))

    {
        $error = true;
    }
    else
    {

        // Values for equipment table
        $equipment_id = $_POST['equipment_id'];
        $customer_id = $_POST['customer_id'];
        $technician = $_POST['technician'];
        $device = $_POST['device'];
        $operation_system = $_POST['operation_system'];
        $fault = $_POST['fault'];
        $system_check = $_POST['system_check'];
        $bios = $_POST['bios'];
        $hardware_test = $_POST['hardware_test'];
        $repaired = $_POST['repaired'];
        $part_replaced = $_POST['part_replaced'];
        $paid = $_POST['paid'];
        $date_hand_in = $_POST['date_hand_in'];
        $date_hand_out = $_POST['date_hand_out'];
        $receipt = $_POST['receipt'];

        $data = $equipment_id . ',' .
            $customer_id . ',' .
            $technician . ',' .
            $device . ',' .
            $operation_system . ',' .
            $fault . ',' .
            $system_check . ',' .
            $bios . ',' .
            $hardware_test . ',' .
            $repaired . ',' .
            $part_replaced . ',' .
            $paid . ',' .
            $date_hand_in . ',' .
            $date_hand_out . ',' .
            $receipt;

        //$data = array($equipment_id , $customer_id , $technician , $device , $operation_system , $fault , $system_check , $bios , $hardware_test , $repaired , $part_replaced , $paid , $date_hand_in , $date_hand_out , $receipt);
        $file = 'equipment_form.csv';

        //Old script code to write $data into the $file csv. document.
        //file_get_contents($file , $data . PHP_EOL , FILE_APPEND);

        //New script code to wrtie $data into the $file csv document.
        $fp = fopen($file, 'a+');
        fwrite($fp,$data.PHP_EOL);
        fclose($fp);

        echo 'Thanks to have used our equipment fault log.';
    }

            if (isset($error)){ ?>
                <div><h1>You must fill out the form !!!! </h1></div>
            <?php }
    }
