<?php
session_start();

require_once 'classes/DB.class.php';
require_once '../helpers/helper.php';
require_once 'classes/admin.class.php';

//Instanciate the admin class
$admin = admin::getInstance();
/*
 **************************************************************
 ** This code is reserved only for the website administrator **
***************************************************************
*/

//session key-value pair given from log_in.php script
if (isset($_SESSION["adminAccess"]))
{
    echo '<a href="admin&logout=1" id="flog_out">LOG OUT</a>';
    
    if(filterPOST('update'))
    {
        $updateUsername = trim(escape($_POST['updateUsername']));
        $updatePassword = trim(escape($_POST['updatePassword']));
        $admin->update($updateUsername, $updatePassword, 'admin');
    }
    elseif(filterPOST('insert'))
    {
        $insertTitle = trim(escape($_POST['title']));
        $insertLink = trim((escape($_POST['link'])));
        $admin->rssFeed($insertTitle, $insertLink, 'rss');
    }
}
else 
{
    //Filtering super global server 
    if(filterSERVER('REQUEST_METHOD') == 'POST')
    {
        //Escaping user input
        $username = escape($_POST['username']);
        $password = escape($_POST['password']);
        $admin->query($username, $password,'admin');
    }
    
    //echo message and spit out footer
    echo '<div class="authenticated">
                <p class="authenticated">
                    Only the website administrator is allowed to access beyond this area.
                </p>
          </div>';
?>

<div style="text-align: center">
    <form action="<?php filterSERVER('PHP_SELF'); ?>" method="POST" name="admin">
        <label for="username">Username:</label> <input type="text" name="username" id="username" />
        <label for="password" >Password:</label> <input type="password" name="password" id="password" />
        <input type="submit" name="adminSubmit" value="Access!" />
    </form>
</div>
    </div> <!-- end of page div -->
    
    <footer>

        <p class="footerpara">
            Copyright &#169; 2014 all rights reserved.
            No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
            By accessing any information beyond this page, you agree to abide by this Privacy Policy.
        </p>

    </footer> <!-- end of footer -->

<?php
    
    exit;
}
?>