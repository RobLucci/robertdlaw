<?php
require_once '../helpers/helper.php';
require_once 'DB.class.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author roblucci
 */
class admin extends DB
{
    private $_query; //store the query
    private $_insert; //store the query insert
    private $_update; //store the query update
    private $_show;
    
    //the 
    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function query($username, $password, $table)
    {      
        $this->_query = $this->_connect->query("SELECT * FROM `{$table}`");
        if($this->_query->rowCount() == 1)
        {
            $result = $this->_query->fetch(PDO::FETCH_OBJ);
            $salt1 = '*/d2';
            $salt2 = '-rl$';
            if($result->username == $username && $result->password == sha1("$salt1$password$salt2"))
            {
                $_SESSION['adminAccess'] = 'true';
                reroute('admin');
            }
            else
            {
                output('You are not the admin');
            }
        }
    }
    
    public function update($updateUsername, $updatePassword, $table)
    {     
        if(empty($updateUsername) || empty($updatePassword))
        {
            output('Username and password cannot be empty');
        }
        else
        {
            $salt1 = '*/d2';
            $salt2 = '-rl$';
            $token = sha1("$salt1$updatePassword$salt2");
            $this->_update = $this->_connect->prepare("UPDATE $table SET `username`= ?, `password`= ? WHERE `id`= 1 ");
            $this->_update->execute(array($updateUsername , $token));
            
            output("Updated username: $updateUsername and password: $updatePassword");
        }
    }
    
    public function rssFeed($insertTitle, $insertLink, $table)
    {     
        if(empty($insertTitle) || empty($insertLink))
        {
            output('Title and/or link cannot be empty');
        }
        else
        {
            $check1 = $this->_connect->query("SELECT `title` FROM `{$table}` WHERE `title` ='" .$insertTitle. "'");
            $check2 = $this->_connect->query("SELECT `title` FROM `{$table}` WHERE `link` ='" .$insertLink. "'");
            if($check1->rowCount($check1) > 0)
            {
                output('Title already taken');
            }
            elseif($check2->rowCount($check2) > 0)
            {
                output('Link already taken');
            }
            else
            {
                $this->_insert = $this->_connect->prepare("INSERT INTO $table(`title`, `link`) VALUES(?,?)");
                $this->_insert->execute(array($insertTitle,$insertLink));
                
                output("Inserted title: $insertTitle and link: $insertLink");
            }
        }
    }
    
    public function rssShow()
    {
        $this->_show = $this->_connect->query("SELECT * FROM `rss`") or die("Could not retrive rssFeeds from database");
        if($rows = $this->_show->rowCount())
        {
            for($j = 0; $j < $rows; $j++)
            {
                $row = $this->_show->fetch(PDO::FETCH_OBJ);
                echo <<<_END
                    <table>
                        <tr>
                            <th>Title</th>
                            <td>$row->title</td>
                        </tr>
                        <tr>
                            <th>Link</th>
                            <td>$row->link</td>
                        </tr>
                        <tr>
                            <td colspan = '2'>
                                <form action='feedDashBoard' method='POST'>
                                <input type='hidden' name='delete' value='yes' />
                                <input type='hidden' name='title' value="$row->title" />
                                <input type='submit' name='submit' value='Delete rssfFeed' /> 
                                </form>
                            </td>
                        </tr>
                    </table>
                        <br />
                        <br />
_END;
            } 
        }
    }
    
    public function rssDelete($title)
    {
        $this->_connect->query("DELETE FROM `rss` WHERE `title`= '$title'");
    }
}
?>
        