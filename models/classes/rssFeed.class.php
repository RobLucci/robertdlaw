<?php

require_once '../helpers/helper.php';
require_once 'DB.class.php';

class rss extends DB
{
    private $_xml; // store the xml DOM file
    private $_select; //store query for option titles to display 
    private $_delect;

    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function rssFeed($title)
    {
        $this->_xml = $this->_connect->query("SELECT `link` FROM `rss` WHERE `title`= '" .$title. "'");
        if($title == 'choose')
        {
            // Display that message if thee user does not select anything
            echo '<div id="rssChoose">'.'Please choose from one of the given RSS news feed'.'</div>';
        }
        else
        {
            if($this->_xml->rowCount() == 0)
            {
                // Display message if user manipulate the GET variable form the URL 
                echo '<div id="rssChoose">'.'Sorry but the rss chosen is not present'.'</div>';
            }
            else
            {
                //fetches xml url if present in the table
                $xml = $this->_xml->fetch(PDO::FETCH_OBJ);
                // Download xml file if title is present in the database
                $dom = simplexml_load_file($xml->link);

                echo "<div id='rss'>";
                echo "<h3 class='xml'>" . $dom->channel->title . "</h3>";

                foreach($dom->channel->item as $item)
                {
                    print "<li class='xml'>";
                    print "<a class='xml' href= '{$item->link}' target='_blank' >";
                    print $item->title;
                    print "</a>";
                    print "</li>";
                }
                echo '</div>';
            }
        }
    }
    
    public function rssOption()
    {
        $this->_select = $this->_connect->query("SELECT `title` FROM `rss`");
        if($this->_select)
        {
            echo "<option selected='selected' value ='choose'>";
            echo "Select one newsFeed from...";
            echo "</option>";
            //Loop through rss titles
            $options = $this->_select->rowCount();
            for($j = 0 ; $j < $options; $j++)
            {
                $option= $this->_select->fetch(PDO::FETCH_OBJ);
                echo "<option value='$option->title'>";
                echo $option->title;
                echo "</option>";
            }
        }
        else
        {
            echo "<option selected = 'selected'>";
            echo "DB's down";
            echo "</option>";
        }
    }

    public function rrsDelete()
    {
        
    }
}
?>