<?php
if(isset($_POST['delete']) && isset($_POST['title']))
{
    $title = escape($_POST['title']);
    
    $admin = admin::getInstance();
    $admin->rssDelete($title);
    
    output('rssFeed with title: '.$title.' has been deleted');
    
}

?>