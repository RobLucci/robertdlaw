<?php
session_start();

//session key-value pair given from log_in.php script
if (isset($_SESSION["authenticated"]))
{
    echo '<a href="log_in&logout=1" id="flog_out">LOG OUT</a>';
}
else
{
    if(filterSERVER('REQUEST_METHOD') == 'POST')
    {
        define('SECRET', 'anything');
        //Escaping user input
        $secret = escape($_POST['secretPassword']);
        if($secret != SECRET)
        {
            output('Wrong secret');
        }
        else
        {
            $_SESSION['authenticated'] = true;
            reroute('cv');
        }
    }
    //echo message and spit out footer

    echo '<div class="authenticated">
                <p class="authenticated">
                    Please <a href="mail_form">Contact me</a> to view this page
                </p>
          </div>';
?>

    <div style="text-align: center">
        <form action="<?php filterSERVER('PHP_SELF'); ?>" method="POST" name="cv">
            <label for="secret"> Secret: </label> <input type="password" name="secretPassword" id="secret" />
            <input type="submit" name="secret" value="Secret!" />
        </form>
    </div>

</div> <!-- end of page div -->

    <footer>

        <p class="footerpara">
            Copyright &#169; 2014 all rights reserved.
            No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
            By accessing any information beyond this page, you agree to abide by this Privacy Policy.
        </p>

    </footer> <!-- end of footer -->

<?php

 exit;

}
?>