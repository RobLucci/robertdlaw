<div id="container">

<script type="text/javascript" src="js/validationForm.js"></script>

    <table class="mail">

    <!-- Email form for visitors-->
        <form action="<?php $_SERVER['PHP_SELF']; ?>" id="mail_form" name="mail_form" method="POST" onsubmit="return validateForm()" >
            <thead>
                <tr>
                    <th colspan="2">
                        EMAIL FORM
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="even">
                    <td>
                        <label>
                            <p> Sender First Name:
                                <input type="text" value="<?php if(isset($_POST['fname'])) echo htmlspecialchars($_POST['fname']); ?>" name="fname" size="30"/>
                            </p>
                        </label>
                    </td>
                    <td>
                        <label>
                            <p> Sender Last Name:
                                <input type="text" value="<?php if(isset($_POST['lname'])) echo htmlspecialchars($_POST['lname']); ?>" name="lname" size="30"/>
                            </p>
                        </label>
                    </td>
                </tr>
                <tr class="odd">
                    <td colspan="2">
                        <label>
                            <p> Your E-mail:
                                <input type="text" value="<?php if(isset($_POST['email'])) echo htmlspecialchars($_POST['email']); ?>" name="email" size="30"/>
                            </p>
                        </label>
                    </td>
                </tr>
                <tr class="even">
                    <td colspan="2">
                        <label>
                            <p> Subject:
                                <input type="text" value="<?php if(isset($_POST['subject'])) echo htmlspecialchars($_POST['subject']); ?>" name="subject" size="30"/>
                            </p>
                        </label>
                    </td>
                </tr>
                <tr class="odd">
                    <td colspan="2">
                        <label for="text"> Text: </label>
                        <p>
                            <textarea id="text" name="textarea" value="<?php if(isset($_POST['textarea'])) echo htmlspecialchars($_POST['textarea']); ?>">
                            </textarea>
                        </p>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="even">
                    <td colspan="2">
                        <input type="submit" id="send" name="submit" value="Send"/>
                    </td>
                </tr>
            </tfoot>
        </form>
    </table>

<script type="text/javascript" src="js/mailFocus.js"></script>

</div><!-- end of container div -->