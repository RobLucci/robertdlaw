<div id="container">

    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">

        <p> Customer ID:
            <input type="text" name="customer_id" value= "<?php if (isset($_POST["customer_id"])) echo htmlspecialchars($_POST["customer_id"]); ?>" size="10" />
        </p>
        <p> First Name:
            <input type="text" name="first_name" value= "<?php if (isset($_POST["first_name"])) echo htmlspecialchars($_POST["first_name"]); ?>" size="30"/>
        </p>
        <p> Last Name:
            <input type="text" name="last_name" value= "<?php if (isset($_POST["last_name"])) echo htmlspecialchars($_POST["last_name"]); ?>" size="30"/>
        </p>
        <p> Address:
            <input type="text" name="address" value= "<?php if (isset($_POST["address"])) echo htmlspecialchars($_POST["address"]); ?>" size="30"/>
        </p>
        <p> E-mail:
            <input type="text" name="email" value= "<?php if (isset($_POST["email"])) echo htmlspecialchars($_POST["email"]); ?>" size="30"/>
        </p>
        <p> City:
            <input type="text" name="city" value= "<?php if (isset($_POST["city"])) echo htmlspecialchars($_POST["city"]); ?>" size="30"/>
        </p>
        <p> Phone number:
            <input type="text" name="phone_number" value= "<?php if (isset($_POST["phone_number"])) echo htmlspecialchars($_POST["phone_number"]); ?>" size="11"/>
        </p>
        <input type="submit" name="submit" value="Submit"/>
        <input type="reset" name="reset" value="Reset"/>

    </form> <!--end of form -->

</div><!-- end of container div -->
