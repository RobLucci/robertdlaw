<div id="container">

    <form action="<?php $_SERVER["PHP_SELF"] ?>" method="POST">

        <p> Equipment ID:
            <input type="text" name="equipment_id" value="<?php if (isset($_POST["equipment_id"])) {echo htmlspecialchars($_POST["equipment_id"]);} ?>" size="100"/>
        </p>
        <p> Customer ID:
            <input type="text" name="customer_id" value="<?php if (isset($_POST["customer_id"])) {echo htmlspecialchars($_POST["customer_id"]);} ?>" size="6"/>
        </p>
        <!-- technician drop down menu -->
        <p> Technician:
            <select name="technician">
                <option value="Robert"> Robert </option>
                <option value="Eli"> Eli </option>
                <option value="John"> John </option>
            </select>
        </p>
        <!-- Device drop down menu -->
        <p> Device:
            <select name="device">
                <option value="Desktop"> Desktop </option>
                <option value="Chromebook"> Chromebook </option>
                <option value="Tablet"> Tablet </option>
                <option value="Smarthphone"> Smarthphone </option>
                <option value="Console"> Console </option>
                <option value="Notebook"> Notebook </option>
                <option value="Printer"> Printer </option>
                <option value="MP3"> MP3 </option>
                <option value="Handheld"> Handheld </option>
                <option value="Other"> Other </option>
            </select>
        </p>
        <!-- Operation system drop down menu -->
        <p> Operation System:
            <select name="operation_system">
                <option value="Windows 98"> Windows 98 </option>
                <option value="Windows 2000"> Windows 2000 </option>
                <option value="Windows XP"> Windows XP </option>
                <option value="Windows vista"> Windows Vista </option>
                <option value="Windows nero"> Windows Nero </option>
                <option value="Windows 7"> Windows 7 </option>
                <option value="Windows 8/8.1"> Windows 8/8.1 </option>
                <option value="Chromebook"> Chromebook </option>
                <option value="Ubuntu"> Ubuntu </option>
                <option value="Linux"> Linux </option>
                <option value="Android"> Android </option>
                <option value="iOS"> iOS </option>
                <option value="Windows Phone"> Windows Phone </option>
                <option value="BlackBerry"> BlackBerry </option>
                <option value="Sony"> Sony </option>
                <option value="Xbox"> Xbox </option>
                <option value="Proprietary"> Proprietary </option>
            </select>
        </p>

        <p> Fault:
            <input type="text" name="fault" size="100"/>
        </p>

        <!-- Booleam value yes/no -->
        <p> System check:
            <select name="system_check">
                <option value="yes"> YES </option>
                <option value="no"> NO </option>
            </select>
        </p>
        <p> BIOS:
            <select name="bios">
                <option value="yes"> YES </option>
                <option value="no"> NO </option>
            </select>
        </p>
        <p> Hardware test:
            <select name="hardware_test">
                <option value="yes"> YES </option>
                <option value="no"> NO </option>
            </select>
        </p>
        <p> Repaired:
            <select name="repaired">
                <option value="yes"> YES </option>
                <option value="no"> NO </option>
            </select>
        </p>

        <!-- Drop down menu part replaced -->
        <p> Part replaced
            <select name="part_replaced">
                <option value="Empty"> None </option>
                <option value="CPU"> CPU </option>
                <option value="SPU"> SPU </option>
                <option value="Motherboard"> Motherboard </option>
                <option value="Network_card"> Network card </option>
                <option value="GPU"> GPU </option>
                <option value="Memory"> Memory </option>
                <option value="Storage"> Storage </option>
                <option value="Speaker"> Speaker </option>
                <option value="Monitor"> Monitor </option>
                <option value="Case"> Case </option>
                <option value="Cable"> Cable </option>
                <option value="Vans"> Vans </option>
                <option value="Display"> Display </option>
                <option value="Peripheral"> Peripheral </option>
            </select>
        </p>

        <!-- Drop down menu for whether the item has been paid or not-->
        <p> Paid?:
            <select name="paid">
                <option value="yes"> YES </option>
                <option value="no"> NO </option>
            </select>
        </p>

        <p> Date hand in:
            <input type="date" name="date_hand_in" value="dd/mm/yyyy"/>
        </p>
        <p> Date hand out:
            <input type="date" name="date_hand_out" value="dd/mm/yyyy"/>
        </p>

        <p> Receipt:
            <input type="text" name="receipt" size="10"/>
        </p>

        <input type="submit" name="submit" value="Submit equipment"/>
        <input type="reset" name="reset" value="Reset"/>
    </form><!-- end of form -->

</div><!-- end of container div -->