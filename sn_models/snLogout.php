<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();

if(isset($_SESSION['username']))
{
    $sn->logout();
?>
<div class="main">You have been logged out. Please <a href="nestim">click here</a> to refresh the screen

<?php
}
else
{
?>

    <div class="main">You cannot log out because you are not logged in.

<?php
}
?>

</div>