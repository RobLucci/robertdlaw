<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();

$sn->loggedin();

if(isset($_POST['text']))
{
    $text = $_POST['text'];
    $sn->text($text);
}
else
{
    $sn->text2();
}

$sn->text = preg_replace('/\s\s+/', ' ', $sn->text);

if(isset($_FILES['image']['name']))
{
    $sn->photoProfile();
}
?>