<?php
include_once '../helpers/helper.php';
include_once '../sn_models/sn_classes/socialNetwork.class.php';

$sn = socialNetwork::getInstance();

if(filterPOST('login'))
{
    $username = trim(escape($_POST['username']));
    $password = trim(escape($_POST['password']));
    
    $sn->login($username, $password);
}


?>