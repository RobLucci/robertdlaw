<?php

require_once '../sn_helpers/snHelper.php';
require_once '../helpers/helper.php';
require_once 'DB.class.php';


class socialNetwork extends DB
{
    public $appName = "NesTim";// name of the web application 
    
    public  $loggedin, // boolean value set to TRUE or FALSE to check if user is logged in
            $userstr, // default to 'Guest'. Store username value when user logs in
            $username, // store username in the super global session username
            $token, // store salted password in the super global session password [not working properly]
            $error, // handle errror messages during user log in
            $text,
            $name,
            $user1, $user2, $user3, 
            $view,
            $friendChecker, // boolean value. Outputs message if user does not have friend yet.
            $num; //store messages counter
    
    
    private $_result, // PDO query object
            $_preapre, // store prepared PDO query object 
            $_mutual, //array containing users who are following and followed by the session username
            $_followers, //array containig users who follow the session uersnae
            $_following; //array containi users who are being followed by the session username

    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    //Shows user's profile
    public function showProfile($username)
    {
        if(file_exists("photo/$username.jpg"))
        {
            echo "<img src='photo/$username.jpg' />";
        }

        $this->_result = $this->_connect->query("SELECT * FROM `profile` WHERE `username`='".$username."'");

        if($this->_result->rowCount())
        {
            $row = $this->_result->fetch(PDO::FETCH_OBJ);
            echo "<span id='text'>".stripslashes($row->text)."</span>";
        }
    }
    
    public function header()
    {        
        if(isset($_SESSION['username']))
        {
            $this->username = $_SESSION['username'];
            $this->loggedin = TRUE;
            $this->userstr = " ($this->username)";
        }
        else
        {
            $this->loggedin = FALSE;
        }
    }
    
    public function loggedin()
    {
        if(!$this->loggedin)
        {
            echo "You cannot access this part of the site if you are not logged in";
            echo "<div id='footer'>
                    <p class='footerpara'>
                        Copyright &#169; 2014 all rights reserved.
                        No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
                        By accessing any information beyond this page, you agree to abide by this Privacy Policy.
                    </p>
                </div>
            </body>
        </html>";
            die();
        }
    }
    
    public function signup($username, $firstname, $lastname, $password, $repassword, $email)
    {
        //check if any field in the POST array is empty
        if(empty($username) || empty($firstname) || empty($lastname) || empty($password) || empty($repassword) || empty($email))
        {
            snOutput('All the fields must be entered');
        }
        elseif($password != $repassword) //check consinstency of password
        {
            SnOutput('The two given passwords do not match');
        }
        else
        {
            //check if the username or email are already been taken
            $register1 = $this->_connect->query('SELECT `id` FROM `users` WHERE `username` = "'.$username.'"');
            $register2 = $this->_connect->query('SELECT `id` FROM `users` WHERE `email` = "'.$email.'"');
            if($register1->rowCount($register1) > 0)
            {
                snOutput('The username: '.$username.' has already been taken');
            }
            elseif($register2->rowCount($register2) > 0)
            {
                snOutput('The email: '.$email.' has already been taken');
            }
            else
            {
                //prepare SQL 
                $this->_registration = $this->_connect->prepare(
                        'INSERT INTO `users`(username, firstname, lastname, password, email, created) VALUES (?, ?, ?, ?, ?, NOW())'
                        );
                // salting password 
                $salt1 = "qm&h*";
                $salt2 = "pg!@";
                $token = sha1("$salt1$password$salt2");
                
                //execute SQL
                $this->_registration->execute(array($username, $firstname, $lastname, $token, $email));

                snOutput('You have been registered with the username: '.$username);
            }
        }
    }
    
    public function snCheckUser($username)
    {
        $sUsername = escape($username);
        
        $check = $this->_connect->query("SELECT * FROM `users` WHERE `username` = '".$sUsername."'");
        if($check->rowCount())
        {
            echo "<span class='taken'>&#x2718 Sorry, this username has been taken </span>";
        }
        else
        {
            echo "<span class='available'>&#x2714 This username is available </span>";
        }
    }
    
    public function login($username, $password)
    {
        if($username == '' || $password == '')
        {
            $this->error = "<span class='error'>Not all fields were entered</span>";
        }
        else
        {
            $salt1 = "qm&h*";
            $salt2 = "pg!@";
            $token = sha1("$salt1$password$salt2");
            
            $this->_result = $this->_connect->query("SELECT `username`,`password` FROM `users` WHERE `username` = '".$username."' AND `password` = '".$token."'");
            
            if(!$this->_result->rowCount())
            {
                echo "<span class='error'>Username/Password invalid</span>";
            }
            else
            {
                $_SESSION['username'] = $username;
                $_SESSION['password'] = $token;
                reroute('nestim');
            }
        }
    }
    
    public function text($text)
    {
        $text = escape($text);
        $text = preg_replace('/\s\s+/', ' ', $text);
        
        $this->_result = $this->_connect->query("SELECT * FROM `profile` WHERE `username` = '".$this->username."'");
        if($this->_result->rowCount())
        {
            $this->_prepare = $this->_connect->prepare("UPDATE `profile` SET `text`= ? WHERE `username`= ?");
            $this->_prepare->execute(array($text, $this->username));
        }
        else
        {
            $this->_prepare = $this->_connect->prepare("INSERT INTO `profile`(username, text) VALUE(?,?)");
            $this->_prepare->execute(array($this->username, $text));
        }
    }
    
    public function text2()
    {
        $this->_result = $this->_connect->query("SELECT * FROM `profile` WHERE `username` = '".$this->username."'");
        if($this->_result->rowCount())
        {
            $row = $this->_result->fetch(PDO::FETCH_OBJ);
            $text = stripslashes($row->text);
        }
        else
        {
            $this->text = '';
        }
    }
    
    public function photoProfile()
    {
        $saveto = "$this->username.jpg";
        move_uploaded_file($_FILES['image']['tmp_name'],"photo/$saveto");
        $typeok = TRUE;
        
        switch ($_FILES['image']['type'])
        {
            case "image/gif":   $src = imagecreatefromgif("photo/$saveto");  break;
            case "image/jpeg":  // Allow both regular and progressive jpegs
            case "image/pjpeg": $src = imagecreatefromjpeg("photo/$saveto"); break;
            case "image/png":   $src = imagecreatefrompng("photo/$saveto");  break;
            default :           $typeok = FALSE;                     break;
        }
        
        if($typeok)
        {
            list($w, $h) = getimagesize("photo/$saveto");
            
            $max = 100;
            $tw = $w;
            $th = $h;
            if($w > $h && $max < $w)
            {
                $th = $max / $w * $h;
                $tw = $max;
            }
            elseif($h > $w && $max < $h)
            {
                $tw = $max / $h * $w;
                $th = $max;
            }
            elseif($max < $w)
            {
               $tw = $th = $max; 
            }
            $tmp = imagecreatetruecolor($tw, $th);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $tw, $th, $w, $h);
            imageconvolution($tmp, array(array(-1, -1, -1),array(-1, 16, -1), array(-1, -1, -1)), 8, 0);
            imagejpeg($tmp,"photo/$saveto");
            imagedestroy($tmp);
            imagedestroy($src);
        }
    }
    
    public function view($view)
    {
        $this->_result = $this->_connect->query("SELECT `id` FROM `users` WHERE `username`='".$view."'");
        if(!$this->_result->rowCount())
        {
            echo "no user found";
        }
        else
        {
            if($view == $this->username)
            {
                $this->name = "Your";
            }
            else
            {
                $this->name = "$view's";
            }
            echo "<h3>$this->name Profile</h3>";
            $this->showProfile($view);
            echo "<a class='button' href=messages?view=$view>View $this->name message</a>";
        }
    }
    
    public function add($add)
    {
        $this->_result = $this->_connect->query("SELECT * FROM `friends` WHERE `username`='".$add."' AND `friend`='".$this->username."'");
        if(!$this->_result->rowCount())
        {
            $this->_preapre = $this->_connect->prepare("INSERT INTO `friends`(username, friend) VALUE(?, ?)");
            $this->_preapre->execute(array($add, $this->username));
        }
    }
    
    public function remove($remove)
    {
        $this->_preapre = $this->_connect->prepare("DELETE FROM `friends` WHERE `username`= ? AND `friend`= ? ");
        $this->_preapre->execute(array($remove, $this->username));
    }
    
    public function members()
    {
        $this->_result = $this->_connect->query("SELECT `username` FROM `users` ORDER BY `username`");
        $num = $this->_result->rowCount();
        
        for($j = 0; $j < $num; ++$j)
        {
            $row = $this->_result->fetch(PDO::FETCH_NUM);
            if($row[0] == $this->username)
            {
                continue;
            }
            echo "<ul class ='members'>";
            echo "<li>";
            echo "<a href='members?view=$row[0]'>$row[0]</a>";
            $follow = 'Follow';
            
            $userFollowing = $this->_connect->query("SELECT * FROM `friends` WHERE `username`='".$row[0]."' AND `friend`='".$this->username."'");
            $uFing = $userFollowing->rowCount();
            
            $userFollowed = $this->_connect->query("SELECT * FROM `friends` WHERE `username`='".$this->username."' AND `friend`='".$row[0]."'");
            $uFed = $userFollowed->rowCount();
            
            if($uFing + $uFed > 1)
            {
                echo "&harr; is a mutual friend";
            }
            elseif($uFing)
            {
                echo "&larr; you are following";
            }
            elseif($uFed)
            {
                echo "&rarr; is following you";
            }
            
            if(!$uFing)
            {
                echo " [<a href='members?add=$row[0]'>$follow</a>]";
                echo "</li>";
                echo "</ul>";
            }
            else
            {
                echo " [<a href='members?remove=$row[0]'>Drop</a>]";
                echo "</li>";
                echo "</ul>";
            }
        }
    }
    
    public function friends()
    {      
        if(isset($_GET['view']))
        {
            $this->view = escape($_GET['view']);
            $this->_result = $this->_connect->query("SELECT `id` FROM `users` WHERE `username`='".$this->view."'");
            if(!$this->_result->rowCount())
            {
                echo "no user found";
                die('<div id="footer">
                        <p class="footerpara">
                        Copyright &#169; 2014 all rights reserved.
                        No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
                        By accessing any information beyond this page, you agree to abide by this Privacy Policy.
                    </p>
                    </div></body></html>');
            }
            else
            {
                $this->view = escape($_GET['view']);
            }
        }
        else
        {
            $this->view = $this->username;
        }

        if($this->view == $this->username)
        {
            $this->user1 = $this->user2 = "Your";
            $this->user3 = "You are";
        }
        else
        {
            $this->user1 = $this->user3 = "<a href='members?view=$this->view'>$this->view</a>'s";
            $this->user2 = $this->view."'s";
        }
        
        $this->_followers = array();
        $this->_following = array();
        
        $this->_result = $this->_connect->query("SELECT * FROM `friends` WHERE `username`='".$this->view."'");
        $num = $this->_result->rowCount();
        
        for($j = 0; $j < $num; ++$j)
        {
            $row = $this->_result->fetch(PDO::FETCH_NUM);
            $this->_followers[$j] = $row[1];
        }
        
        $this->_result = $this->_connect->query("SELECT * FROM `friends` WHERE `friend`='".$this->view."'");
        $num = $this->_result->rowCount();

        for($j = 0; $j < $num; ++$j)
        {
            $row = $this->_result->fetch(PDO::FETCH_NUM);
            $this->_following[$j] = $row[0];
        }
        
        $this->_mutual = array_intersect($this->_followers, $this->_following);
        $this->_followers = array_diff($this->_followers, $this->_mutual);
        $this->_following = array_diff($this->_following, $this->_mutual);
        $this->friendChecker = FALSE;  
    }
    
    public function mutual()
    {
        if(sizeof($this->_mutual))
        {
            echo "<div class='subhead'>$this->user1 mutual friend</div>";
            echo "<ul class='friends'>";
            foreach($this->_mutual as $friend)
            {
                echo "<li>";
                echo "<a href='friends?view=$friend'>$friend</a>";
                echo "</li>";
            }
            echo "</ul>";
            $this->friendChecker = TRUE;
        }
    }
    
    public function followers()
    {
        if(sizeof($this->_followers))
        {
            echo "<div class='subhead'>$this->user1 followers </div>";
            echo "<ul class='friends'>";
            foreach($this->_followers as $friend)
            {
                echo "<li>";
                echo "<a href='friends?view=$friend'>$friend</a>";
                echo "</li>";
            }
            echo "</ul>";
            $this->friendChecker = TRUE;
        }
    }
    
    public function following()
    {
        if(sizeof($this->_following))
        {
            echo "<div class='subhead'>$this->user3 following</div>";
            echo "<ul class='friends'>";
            foreach($this->_following as $friend)
            {
                echo "<li>";
                echo "<a href='friends?view=$friend'>$friend</a>";
                echo "</li>";
            }
            echo "</ul>";
            $this->friendChecker = TRUE;
        }
    }
    
    public function messages()
    {
        if(isset($_GET['view']))
        {
            $this->view = escape($_GET['view']);
            $this->_result = $this->_connect->query("SELECT `id` FROM `users` WHERE `username`='".$this->view."'");
            if(!$this->_result->rowCount())
            {
                echo "no user found";
                die('<div id="footer">
                        <p class="footerpara">
                        Copyright &#169; 2014 all rights reserved.
                        No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
                        By accessing any information beyond this page, you agree to abide by this Privacy Policy.
                    </p>
                    </div></body></html>');
            }
            else
            {
                $this->view = escape($_GET['view']);
            }
        }
        else
        {
            $this->view = $this->username;
        }
        
        if(isset($_POST['text']))
        {
            $text = trim(escape($_POST['text']));
            if($text != "")
            {
                $pm = substr(escape($_POST['pm']), 0, 1);
                $time = time();
                $this->_preapre = $this->_connect->prepare("INSERT INTO `messages`(id, auth, recip, pm, time, message) VALUES(?,?,?,?,?,?)");
                $this->_preapre->execute(array(NULL, $this->username, $this->view, $pm, $time, $text));
            }
        }
    }
    
    public function messages2()
    {
        if($this->view == $this->username)
        {
            $this->user1 = $this->user2 = $this->user3 = "Your";
        }
        else
        {
            $this->user1 = "<a href='members?view=$this->view'>$this->view</a>'s";
            $this->user2 = $this->view."'s";
        }
    }
    
    public function messages3()
    {
        if(isset($_GET['erase']))
        {
            $erase = escape($_GET['erase']);
            $this->_preapre = $this->_connect->prepare("DELETE FROM `messages` WHERE `id`=? AND `recip`=?");
            $this->_preapre->execute(array($erase, $this->username));
        }
        
        $this->_result = $this->_connect->query("SELECT * FROM `messages` WHERE `recip`='".$this->view."' ORDER BY `time` DESC");
        $this->num = $this->_result->rowCount();
        
        for($j = 0; $j < $this->num; ++$j)
        {
            $row = $this->_result->fetch(PDO::FETCH_NUM);
            if($row[3] == 0 || $row[1] == $this->username || $row[2] == $this->username )
            {
                echo date('M jS \'y g:ia', $row[4]);
                echo " <a href='messages?view=$row[1]'>$row[1]</a>";
                
                if($row[3] == 0)
                {
                echo " wrote &quot; $row[5] &quot; ";
                }
                else
                {
                    echo " whispered: <span class='whisper'>&quot;$row[5]&quot;</span>";
                }
                
                if($row[2] == $this->username)
                {
                    echo " [<a href='messages?view=$this->view&erase=$row[0]'>erase</a>]";
                }
            }
        }
    }
    
    public function logout()
    {
        destroySession();
    }
}
?>