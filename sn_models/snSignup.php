<?php
require_once 'sn_classes/DB.class.php';
require_once '../sn_helpers/snHelper.php';

$sn = socialNetwork::getInstance();

if(filterPOST('signup'))
{   
    //retrivinga and sanitizing user input from the POST array
    $username = trim(escape($_POST['username']));
    $firstname = trim(escape($_POST['firstname']));
    $lastname = trim(escape($_POST['lastname']));
    $password = trim(escape($_POST['password']));
    $repassword = trim(escape($_POST['repassword']));
    $email = trim(escape($_POST['email']));
    
    //registration of user with the registration class method
    $sn->signup($username, $firstname, $lastname, $password, $repassword, $email);
}
?>