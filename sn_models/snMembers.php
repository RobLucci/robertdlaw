<?php
include_once ('../sn_helpers/snHelper.php');
include_once ('../helpers/helper.php');
$sn = socialNetwork::getInstance();

$sn->loggedin();

if(isset($_GET['view']))
{
    $view = escape($_GET['view']);
    $sn->view($view);
}

if(isset($_GET['add']))
{
    $add = escape($_GET['add']);
    $sn->add($add);
}
elseif(isset($_GET['remove'])) 
{
    $remove = escape($_GET['remove']);
    $sn->remove($remove);
}

?>