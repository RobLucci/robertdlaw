<?php

function snRenderView($view, $data = array()) 
{
    //sanitize argument
    $sView = htmlspecialchars($view);

    $directory = "../sn_views/$sView.php";

    if(file_exists($directory))
    {
        extract($data);
        require($directory);
    }
    else
    {
        echo 'The view:'.' '.$sView.' '.'does not exist.<br />';
    }
}

function snRenderTemplate($template, $data = array()) 
{
    //sanitize argument
    $sTemplate= htmlspecialchars($template);

    $directory = "../sn_templates/$sTemplate.php";

    if(file_exists($directory))
    {
        extract($data);
        require($directory);
    }
    else
    {
        echo 'The template:'.' '.$sTemplate.' '.'does not exist.<br />';
    }
}


function snRenderModel($model, $data = array()) 
{
    //sanitize argument
    $sModel = htmlspecialchars($model);

    $directory = "../sn_models/$sModel.php";

    if(file_exists($directory))
    {
        extract($data);
        require($directory);
    }
    else
    {
        echo 'The model:'.' '.$sModel.' '.'does not exist.<br />';
    }
}

//Destroys a PHP session and clears its data to log user out
function destroySession()
{
    $_SESSION = array();
    if(session_id() != "" || isset($_COOKIE[session_name()]))
    {
        setcookie(session_name(), '', time()-2592000, '/');
    }   
    session_destroy();
}

function snOutput($string) 
{
    echo "<div id=\"output\">" . $string . "</div>";
}

?>