// Validation form rules javascript
function validateForm()
{
    var x = document.forms["mail_form"]["fname"].value;
    if (x == "")
    {
        alert("Please insert your first name.");
        return false;
    }
    var x = document.forms["mail_form"]["lname"].value;
    if (x == "")
    {
        alert("Please insert your last name.");
        return false;
    }
    var x = document.forms["mail_form"]["email"].value;
    if (x == "" )
    {
        alert("Please insert your E-mail address.");
        return false;
    }
    var x = document.forms["mail_form"]["subject"].value;
    if (x == "" )
    {
        alert("E-mail cannot be send without a subject.");
        return false;
    }
    var x = document.forms["mail_form"]["textarea"].value;
    if (x == "" || x < 1)
    {
        alert("Please type-in a text prior sending.");
        return false;
    }
    return true;
}

