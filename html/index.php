<?php
/*@  Robert 
*     
* index.php
** 
*
* MVC module [controller]
*
*/

 require_once('../helpers/helper.php');
 require_once('../sn_helpers/snHelper.php');


$page = escape($_GET['page']);
$page = trim($page, "/");

$page = $page ? $page : "index";
switch($page){

    case 'index':
        renderTemplate('header', array('title' => 'Home Page' ,
                                       'name' => 'description' ,
                                       'content' => "Home page of Robert Owusu Ntim" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Homepage, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim" ));
        renderView('index');
        renderTemplate('footer', array());
        break;

    case 'robert':
        renderTemplate('header', array('title' => 'Robert',
                                       'name' => 'description' ,
                                       'content' => "My personal blog" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Homepage, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim" ));
        renderView('robert');
        renderTemplate('footer', array());
        break;

    case 'mail_form':
        renderTemplate('header', array('title' => 'Contact Me',
                                       'name' => 'description' ,
                                       'content' => "Contact me" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Mail form, Contact me, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim"));
        renderModel('send_mail');
        renderView('mail_form');
        renderTemplate('footer', array());
        break;

    case 'extra':
        renderTemplate('header', array('title' => 'Restricted area',
                                       'name' => 'description' ,
                                       'content' => "Restricted area" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "More, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim"));
        renderView('extra');
        renderTemplate('footer', array());
        break;

    //page under normal user restricted area
    case 'cv':
        renderTemplate('header', array( 'title' => 'CV',
                                        'name' => 'description' ,
                                        'content' => "Robert Owsusu ntim curriculum vitae" ,
                                        'name2' => 'keywords' ,
                                        'content2' => "CV, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                        'name3' => 'robots' ,
                                        'content3' => 'nofollow' ,
                                        'httpequiv' => 'author' ,
                                        'content4' => "Robert Owusu Ntim"));
        renderModel('authenticated');
        renderView('cv');
        renderTemplate('footer', array());
        break;

    //page under normal user restricted area
    case 'cleveland':
        renderTemplate('header', array( 'title' => 'Fun',
                                        'name' => 'description' ,
                                        'content' => "Off-line" ,
                                        'name2' => 'keywords' ,
                                        'content2' => "Homepage, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                        'name3' => 'robots' ,
                                        'content3' => 'nofollow' ,
                                        'httpequiv' => 'author' ,
                                        'content4' => "Robert Owusu Ntim"));
        renderView('cleveland');
        renderTemplate('footer', array());
        break;

    case 'admin':
        renderTemplate('header', array( 'title' => 'Admin',
                                        'name' => 'description' ,
                                        'content' => "Restricted area" ,
                                        'name2' => 'keywords' ,
                                        'content2' => "More, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                        'name3' => 'robots' ,
                                        'content3' => 'nofollow' ,
                                        'httpequiv' => 'author' ,
                                        'content4' => "Robert Owusu Ntim"));
        renderModel('adminAuthenticated');
        renderView('admin');
        renderTemplate('footer', array());
        break;
    
    case 'feedDashBoard':
        renderTemplate('header', array('title' => 'rssDash Control',
                                       'name' => 'description' ,
                                       'content' => "Dsh control" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Dash board, Admin, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim"));
        renderModel('feedDashBoard');
        renderView('feedDashBoard');
        renderTemplate('footer');
        break;

    //page under admin restriction
    case 'equipment_form':
        renderTemplate('header', array('title' => 'Equipment Form',
                                       'name' => 'description' ,
                                       'content' => "Eqipment form" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Equipment form , Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim"));
        renderModel('adminAuthenticated');
        renderModel('equipment_form');
        renderView('equipment_form');
        renderTemplate('footer', array());
        break;

    //page under admin restriction
    case 'customer_form':
        renderTemplate('header', array('title' => 'Customer Form',
                                       'name' => 'description' ,
                                       'content' => "Customer form" ,
                                       'name2' => 'keywords' ,
                                       'content2' => "Customer form, Robert, Lussana, XXL, Basket, robertdlaw, robertdlaw.tk" ,
                                       'name3' => 'robots' ,
                                       'content3' => 'nofollow' ,
                                       'httpequiv' => 'author' ,
                                       'content4' => "Robert Owusu Ntim"));
        renderModel('adminAuthenticated');
        renderModel('customer_form');
        renderView('customer_form');
        renderTemplate('footer', array());
        break;
    
    /*
        Social network cases
    */
    
    case 'nestim':
        snRenderTemplate('snHeader');
        snRenderView('snIndex');
        snRenderTemplate('snFooter');
        break;
    
    case 'signup':
        snRenderTemplate('snHeader');
        snRenderModel('snSignup');
        snRenderView('snSignup');
        snRenderTemplate('snFooter');
        break;
    
    case 'login':
        snRenderTemplate('snHeader');
        snRenderModel('snLogin');
        snRenderView('snLogin');
        snRenderTemplate('snFooter');
        break;
    
    case 'profile':
        snRenderTemplate('snHeader');
        snRenderModel('snProfile');
        snRenderView('snProfile');
        snRenderTemplate('snFooter');
        break;
    
    case 'members':
        snRenderTemplate('snHeader');
        snRenderModel('snMembers');
        snRenderView('snMembers');
        snRenderTemplate('snFooter');
        break;
    
    case 'friends':
        snRenderTemplate('snHeader');
        snRenderModel('snFriends');
        snRenderView('snFriends');
        snRenderTemplate('snFooter');
        break;
    
    case 'messages':
        snRenderTemplate('snHeader');
        snRenderModel('snMessages');
        snRenderView('snMessages');
        snRenderTemplate('snFooter');
        break;
    
    case 'logout':
        snRenderTemplate('snHeader');
        snRenderModel('snLogout');
        snRenderTemplate('snFooter');
        break;
}
