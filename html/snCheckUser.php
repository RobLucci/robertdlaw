<?php
//PHP half for the AJAX calls

include_once('../sn_models/sn_classes/socialNetwork.class.php');
include_once ('../helpers/helper.php');

$sn = socialNetwork::getInstance();

if(isset($_POST['username']))
{
    $username = trim(escape($_POST['username']));
    $sn->snCheckUser($username);
}
?>