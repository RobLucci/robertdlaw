<?php
session_start();
include_once ('../sn_helpers/snHelper.php');
include_once ('../sn_models/sn_classes/socialNetwork.class.php');
$sn = socialNetwork::getInstance();

$sn->userstr = " (Guest)";

$sn->header();
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $sn->appName.$sn->userstr; ?></title>
        <link rel="stylesheet" href="../css/snStyle.css" type="text/css" />
        <script type="text/javascript" src="js/OSC.js"></script>
    </head>
    <body>
        <div class="appname"><?php echo $sn->appName.$sn->userstr; ?> <a id="site" href="index"> robertdlaw.tk </a></div>
        <div id="page">
        <?php
            if($sn->loggedin)
            {
        ?>
        <ul class="menu">
            <li><a href='members?view=<?php echo $sn->username; ?>'>Home</a></li>
            <li><a href='members'>Members</a></li>
            <li><a href='friends'>Friends</a></li>
            <li><a href='messages'>Messages</a></li>
            <li><a href='profile'>Edit Profile</a></li>
            <li><a href='logout'>Logout</a></li>
        </ul>
        <?php
            }
            else
            {    
        ?>
        <ul class="menu2">
            <li><a href="nestim">Home</a></li>
            <li><a href="signup">Sign up</a></li>
            <li><a href="login">Log in</a></li>
        </ul>
        <div class="info">&#8658; You must be logged in to view this page</div>
        <?php
            }
        ?>
